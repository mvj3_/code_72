首选下载git-completion.bash

`curl https://raw.github.com/git/git/master/contrib/completion/git-completion.bash -o ~/.git-completion.bash`

然后在`~/.bash_profile`文件最下面添加如下代码:
```sh
if [ -f ~/.git-completion.bash ]; then
  . ~/.git-completion.bash
fi
```

最后就可以用`git checkout [TAB]`来自动补全功能了～


参考资料:
http://apple.stackexchange.com/questions/55875/how-can-i-get-git-to-autocomplete-e-g-branches-at-the-command-line